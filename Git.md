# GIT教程

## 一、GIT基础

### 1.1 Git是什么？

```
简单的说Git是一个开源的分布式版本控制系统。
```

​	如果你用Microsoft Word写过长篇大论，那你一定有这样的经历：想删除一个段落，又怕将来想恢复找不回来怎么办？有办法，先把当前文件“另存为……”一个新的Word文件，再接着改，改到一定程度，再“另存为……”一个新文件，这样一直改下去，最后你的Word文档变成了这样：

<img src="https://gitee.com/daxiang2008/doc/raw/master/Typora/img/image-20210406084434393.png" alt="image-20210406084434393" style="zoom: 80%;" />

**问题:**

​	1、过了一周，你想找回被删除的文字，但是已经记不清删除前保存在哪个文件里了，只好一个一个文件去找。麻烦不？

​	2、看着一堆乱七八糟的文件，想保留最新的一个，然后把其他的删掉，又怕哪天会用上，还不敢删。麻烦不？

​	3、有些部分需要你的同事帮助填写，于是你把文件Copy到U盘里给她（也可能通过Email发送一份给她），然后，你继续修改Word文件。一天后，同事再把Word文件传给你，此时，你必须想想，发给她之后到你收到她的文件期间，你作了哪些改动，得把你的改动和她的部分合并。是不是想死的心都有了！！！

**解决方式：**

​	于是你想，如果有一个软件，不但能自动帮我记录每次文件的改动，还可以让同事协作编辑，这样就不用自己管理一堆类似的文件了，也不需要把文件传来传去。如果想查看某次改动，只需要在软件里瞄一眼就可以，岂不是很方便？

​	这个软件用起来就应该像这个样子，能记录每次文件的改动：

<img src="https://gitee.com/daxiang2008/doc/raw/master/Typora/img/image-20210406085220439.png" alt="image-20210406085220439" style="zoom:50%;" />

你碰到的问题已经被一个人解决了，他就是LinusTorvalds李纳斯·托沃兹（linux的创造者）

<img src="https://gitee.com/daxiang2008/doc/raw/master/Typora/img/image-20210406085303866.png" alt="image-20210406085303866" style="zoom:50%;" />

### 1.2 Git的诞生

​	很多人都知道，Linus在1991年创建了开源的Linux，从此，Linux系统不断发展，已经成为最大的服务器系统软件了。

​	Linus虽然创建了Linux，但Linux的壮大是靠全世界热心的志愿者参与的，这么多人在世界各地为Linux编写代码，那Linux的代码是如何管理的呢？事实是，在2002年以前，世界各地的志愿者把源代码文件通过diff的方式发给Linus，然后由Linus本人通过手工方式合并代码！你也许会想，为什么Linus不把Linux代码放到版本控制系统里呢？不是有CVS、SVN这些免费的版本控制系统吗？因为Linus坚定地反对CVS和SVN，这些集中式的版本控制系统不但速度慢，而且必须联网才能使用。有一些商用的版本控制系统，虽然比CVS、SVN好用，但那是付费的，和Linux的开源精神不符。不过，到了2002年，Linux系统已经发展了十年了，代码库之大让Linus很难继续通过手工方式管理了，社区的弟兄们也对这种方式表达了强烈不满，于是Linus选择了一个商业的版本控制系统BitKeeper，BitKeeper的东家BitMover公司出于人道主义精神，授权Linux社区免费使用这个版本控制系统。安定团结的大好局面在2005年就被打破了，原因是Linux社区牛人聚集，不免沾染了不好的。开发Samba的Andrew试图破解BitKeeper的协议（这么干的其实也不只他一个），被BitMover公司发现了（监控工作做得不错！），于是BitMover公司怒了，要收回Linux社区的免费使用权。Linus可以向BitMover公司道个歉，保证以后严格管教弟兄们，嗯，这是不可能的。

​	实际情况是这样的：Linus花了两周时间自己用C写了一个分布式版本控制系统，这就是Git！一个月之内，Linux系统的源码已经由Git管理了！牛是怎么定义的呢？大家可以体会一下。

​	Git迅速成为最流行的分布式版本控制系统，尤其是2008年，GitHub网站上线了，它为开源项目免费提供Git存储，无数开源项目开始迁移至GitHub，包括jQuery，PHP，Ruby等等。

​	历史就是这么偶然，如果不是当年BitMover公司威胁Linux社区，可能现在我们就没有免费而超级好用的Git了。

### 1.3 什么是版本控制？

​	什么是版本控制？版本控制是一种记录一个或若干文件内容变化，以便将来查阅特定版本修订情况的系统。

​	如果你是ps前端高手，可能会需要保存某一幅照片或页面布局文件的所有修订版本（这或许是你非常渴望拥有的功能）。采用版本控制系统（VCS）是个明智的选择。有了它你就可以将某个文件回溯到之前的状态，甚至将整个项目都回退到过去某个时间点的状态。你可以比较文件的变化细节，查出最后是谁修改了哪个地方，从而找出导致怪异问题出现的原因，又是谁在何时报告了某个功能缺陷等等。使用版本控制系统通常还意味着，就算你乱来一气把整个项目中的文件改的改删的删，你也照样可以轻松恢复到原先的样子。但额外增加的工作量却微乎其微。

​	**你想想系统还原是不是也是这样？**

### 1.4 什么是分布式版本控制系统？

​	Linus一直痛恨的CVS及SVN都是集中式的版本控制系统，而Git是分布式版本控制系统，集中式和分布式版本控制系统有什么区别呢？

​	有必要先了解一下传统的集中式版本控制系统。

​	**集中化的版本控制系统**

​	诸如CVS，SVN等，都有一个单一的集中管理的服务器，保存所有文件的修订版本，而协同工作的人们都通过客户端连到这台服务器，取出最新的文件或者提交更新。重点是：必须有网络。这么做最显而易见的缺点是如果中央服务器的单点故障。如果宕机一小时，那么在这一小时内，谁都无法提交更新，也就无法协同工作。要是服务器的磁盘发生故障，碰巧没做备份，或者备份不够及时，就会有丢失数据的风险。最坏的情况是彻底丢失整个项目的所有历史更改记录。

​	举例：（中央服务器就好比是一个图书馆，你要改一本书，必须先从图书馆借出来，然后回到家自己改，改完了，再放回图书馆。）

<img src="https://gitee.com/daxiang2008/doc/raw/master/Typora/img/image-20210406093005911.png" alt="image-20210406093005911" style="zoom:50%;" />

​																															微软的（vss）

​	分布式版本控制系统的客户端并不只提取最新版本的文件快照，而是把代码仓库完整地镜像下来。这么一来，任何一处协同工作用的中央服务器发生故障，事后都可以用任何一个镜像出来的本地仓库恢复。因为每一次的提取操作，实际上都是一次对代码仓库的完整备份。

<img src="https://gitee.com/daxiang2008/doc/raw/master/Typora/img/image-20210406093057005.png" alt="image-20210406093057005" style="zoom:50%;" />

### 1.5 为什么使用Git？

​	Git是分布式的。这是Git和其它非分布式的版本控制系统，例如svn，cvs等，最核心的区别。分布式带来以下好处：

​	**不需要联网**

​	首先，分布式版本控制系统根本没有“中央服务器”，每个人的电脑上都是一个完整的版本库，这样，你工作的时候，就不需要联网了，因为版本库就在你自己的电脑上。既然每个人电脑上都有一个完整的版本库，那多个人如何协作呢？比方说你在自己电脑上改了文件A，你的同事也在他的电脑上改了文件A，这时，你们俩之间只需把各自的修改推送给对方，就可以互相看到对方的修改了。

​	集中式版本控制系统，一旦中央服务器出了问题，所有人都无法工作。

​	分布式版本控制系统，每个人电脑中都有完整的版本库，更加安全，所以某人的机器挂了，并不影响其它人。

### 1.6 Git基本原理

那么，简单地说，Git究竟是怎样的一个系统呢？请注意，接下来的内容非常重要，若是理解了Git的思想和基本工作原理，用起来就会知其所以然，游刃有余。

在开始学习Git的时候，请不要尝试把各种概念和其他版本控制系统（诸如Subversion和Perforce等）相比拟，否则容易混淆每个操作的实际意义。

Git在保存和处理各种信息的时候，虽然操作起来的命令形式非常相近，但它与其他版本控制系统的做法颇为不同。理解这些差异将有助于你准确地使用Git提供的各种工具。

- **直接记录快照，而非差异比较**

Git和其他版本控制系统的主要差别在于，Git只关心文件数据的整体是否发生变化，而大多数其他系统则只关心文件内容的具体差异。这类系统（CVS，Subversion，Perforce，Bazaar等等）每次记录有哪些文件作了更新，以及都更新了哪些行的什么内容，请看图。

![image-20210407102224624](https://gitee.com/daxiang2008/doc/raw/master/Typora/img/image-20210407102224624.png)

​	Git并不保存这些前后变化的差异数据。实际上，Git更像是把变化的文件作快照后，记录在一个微型的文件系统中。每次提交更新时，它会纵览一遍所有文件的指纹信息并对文件作一快照，然后保存一个指向这次快照的索引。为提高性能，若文件没有变化，Git不会再次保存，而只对上次保存的快照作一链接。Git的工作方式就像下图所示。

![image-20210407102251885](https://gitee.com/daxiang2008/doc/raw/master/Typora/img/image-20210407102251885.png)

​	这是Git同其他系统的重要区别。它完全颠覆了传统版本控制的套路，并对各个环节的实现方式作了新的设计。Git更像是个小型的文件系统，但它同时还提供了许多以此为基础的超强工具，而不只是一个简单的CVS。

- **近乎所有操作都是本地执行**

​	在Git中的绝大多数操作都只需要访问本地文件和资源，不用连网。但如果用CVS的话，差不多所有操作都需要连接网络。因为Git在本地磁盘上就保存着所有当前项目的历史更新，所以处理起来速度飞快。

​	举个例子，如果要浏览项目的历史更新摘要，Git不用跑到外面的服务器上去取数据回来，而直接从本地数据库读取后展示给你看。所以任何时候你都可以马上翻阅，无需等待。如果想要看当前版本的文件和一个月前的版本之间有何差异，Git会取出一个月前的快照和当前文件作一次差异运算，而不用请求远程服务器来做这件事，或是把老版本的文件拉到本地来作比较。

​	用CVS的话，没有网络或者断开你就无法做任何事情。但用Git的话，就算你在飞机或者火车上，都可以非常愉快地频繁提交更新，等到了有网络的时候再上传到远程仓库。同样，在回家的路上，不用连接你也可以继续工作。换作其他版本控制系统，这么做几乎不可能，抑或非常麻烦。比如Perforce，如果不连到服务器，几乎什么都做不了；如果是Subversion或CVS，虽然可以编辑文件，但无法提交更新，因为数据库在网络上。看上去好像这些都不是什么大问题，但实际体验过之后，你就会惊喜地发现，这其实是会带来很大不同的。

- **时刻保持数据完整性**

​	在保存到Git之前，所有数据都要进行内容的校验和（checksum）计算，并将此结果作为数据的唯一标识和索引。换句话说，不可能在你修改了文件或目录之后，Git一无所知。这项特性作为Git的设计哲学，建在整体架构的最底层。所以如果文件在传输时变得不完整，或者磁盘损坏导致文件数据缺失，Git都能立即察觉。

​	Git使用SHA-1算法计算数据的校验和，通过对文件的内容或目录的结构计算出一个SHA-1哈希值，作为指纹字符串。该字串由40个十六进制字符（0-9及a-f）组成，看起来就像是：24b9da6552252987aa493b52f8696cd6d3b00373

​	Git的工作完全依赖于这类指纹字串，所以你会经常看到这样的哈希值。实际上，所有保存在Git数据库中的东西都是用此哈希值来作索引的，而不是靠文件名。

- **多数操作仅添加数据**

​	常用的Git操作大多仅仅是把数据添加到数据库。因为任何一种不可逆的操作，比如删除数据，都会使回退或重现历史版本变得困难重重。在别的VCS中，若还未提交更新，就有可能丢失或者混淆一些修改的内容，但在Git里，一旦提交快照之后就完全不用担心丢失数据，特别是养成定期推送到其他仓库的习惯的话。

​	这种高可靠性令我们的开发工作安心不少，尽管去做各种试验性的尝试好了，再怎样也不会弄丢数据。

### 1.7 文件三种状态

​	现在请注意，接下来要讲的概念非常重要。对于任何一个文件，在Git内都只有三种状态：

- **已修改（modified）**

已修改表示修改了文件，但还没保存到数据库中。

- **已暂存（staged）**

已暂存表示对一个已修改文件的当前版本做了标记，使之包含在下次提交的快照中。

- **已提交（committed）**

已提交表示数据已经安全的保存在本地数据库中。

### 1.8 工作区域

​	由上面文件的三种状态，我们看到Git管理项目时，文件流转的三个工作区域：Git的工作区，暂存区，以及本地仓库。

![image-20211005151335309](https://gitee.com/daxiang2008/doc/raw/master/Typora/img/image-20211005151335309.png)

- **工作区（working）**

​	当你gitclone一个项目到本地，相当于在本地克隆了项目的一个副本。

​	工作区是对项目的某个版本独立提取出来的内容。这些从Git仓库的压缩数据库中提取出来的文件，放在磁盘上供你使用或修改。

- **暂存区（staging）**

​	暂存区是一个文件，保存了下次将提交的文件列表信息，一般在Git仓库目录中。有时候也被称作`‘索引’'，不过一般说法还是叫暂存区。

- **本地仓库（local）**

​	提交更新，找到暂存区域的文件，将快照永久性存储到Git本地仓库。

​	以上几个工作区都是在本地。为了让别人可以看到你的修改，你需要将你的更新推送到远程仓库。

- **远程仓库（remote）**

​	同理，如果你想同步别人的修改，你需要从远程仓库拉取更新。

![image-20211005151512227](https://gitee.com/daxiang2008/doc/raw/master/Typora/img/image-20211005151512227.png)

## 二、Git配置安装

### 2.1 Git安装

在https://git-scm.com/下载git for windows，下载windows版本的git安装包

（1）单击Next

![image-20210407100754525](https://gitee.com/daxiang2008/doc/raw/master/Typora/img/image-20210407100754525.png)

（2）选择安装目录

![image-20210407100832765](https://gitee.com/daxiang2008/doc/raw/master/Typora/img/image-20210407100832765.png)

（3）勾选创建桌面快捷方式、Git Bash、Git GUi、已经目录和后缀关联等，如图。

![image-20210407100841009](https://gitee.com/daxiang2008/doc/raw/master/Typora/img/image-20210407100841009.png)

（4）默认即可，单击Next

![image-20210407100847765](https://gitee.com/daxiang2008/doc/raw/master/Typora/img/image-20210407100847765.png)

（5）在“Adjusting your PATH environment”选项中，默认选项是“Use Git from the Windows Command Prompt”，这样在Windows的命令行cmd中也可以运行git命令了，点击“Next”。

![image-20210407100904118](https://gitee.com/daxiang2008/doc/raw/master/Typora/img/image-20210407100904118.png)

（6）配置行结束标记，保持默认“Checkout Windows-style, commit Unix-style line endings”。

![image-20210407100914355](https://gitee.com/daxiang2008/doc/raw/master/Typora/img/image-20210407100914355.png)

（7）在终端模拟器选择页面,默认即可，配置后Git Gash的终端比较易用。然后点击“Next”按钮

![image-20210407100922691](https://gitee.com/daxiang2008/doc/raw/master/Typora/img/image-20210407100922691.png)

（8）最后配置Git额外选择默认即可。

![image-20210407100932297](https://gitee.com/daxiang2008/doc/raw/master/Typora/img/image-20210407100932297.png)

![image-20210407100939072](https://gitee.com/daxiang2008/doc/raw/master/Typora/img/image-20210407100939072.png)

（9）安装成功

![image-20210407100947115](https://gitee.com/daxiang2008/doc/raw/master/Typora/img/image-20210407100947115.png)

（10）安装Git完成之后，会在桌面创建Git Bash快捷方式，在任意目录下右击鼠标可以找打Git Bash Here的选项。

![image-20210407101002003](https://gitee.com/daxiang2008/doc/raw/master/Typora/img/image-20210407101002003.png)

### 2.2 Git配置

​	Git客户端在安装完成后，需要设置一个位置用于与存放默认的仓库。

​	1、**设置GitHOME：**

​		需要在系统环境变量中为Git提供一个HOME路径。这个目录将用于保存git的配置文件和默认的本地仓库。

​	我这里使用的是F:\GitHome，当然你可以设置系统盘的任意路径。

![image-20210407101240258](https://gitee.com/daxiang2008/doc/raw/master/Typora/img/image-20210407101240258.png)

​	2、**配置用户名和邮箱**

​		当安装完 Git 应该做的第一件事就是设置你的用户名称与邮件地址。 这样做很重要，因为每一个 Git 的提交都会使用这些信息，并且它会写入到你的每一次提交中：

```bash
$ git config --global user.name "你的名字或昵称" 
$ git config --global user.email "你的邮箱"
```

​		查看配置信息

```bash
$ git config --list
```

​		如果想修改昵称或者邮箱，可以再次配置一遍即可！

## 三、Git基础命令

### 3.1 创建本地仓库

​	在任意位置创建一个文件夹，在控制台进入该文件夹路径。

```bash
$ git init
```

### 3.2 添加文件

​	在该文件夹下创建一个“readme.txt”的文本文件。（任意格式的文件都可以）

```bash
$ git add readme.txt
```

​	执行上面的命令，没有任何显示，这就对了，Unix的哲学是“没有消息就是好消息”，说明添加成功。然后在控制台用命令git commit告诉Git，把文件提交到仓库：简单解释一下git commit命令，-m后面输入的是本次提交的说明，可以输入任意内容，当然最好是有意义的，这样你就能从历史记录里方便地找到改动记录。

![image-20210407160536487](https://gitee.com/daxiang2008/doc/raw/master/Typora/img/image-20210407160536487.png)

### 3.3 提交记录

​	用命令`git commit`告诉Git，把文件提交到仓库：

```bash
$ git commit -m 增加readme.txt
```

![image-20210407160516020](https://gitee.com/daxiang2008/doc/raw/master/Typora/img/image-20210407160516020.png)

```bash
# 提交暂存区到仓库区
$ git commit -m [message]

# 提交暂存区的指定文件到仓库区
$ git commit [file1] [file2] ... -m [message]

# 提交工作区自上次commit之后的变化，直接到仓库区
$ git commit -a

# 提交时显示所有diff信息
$ git commit -v

# 使用一次新的commit，替代上一次提交
# 如果代码没有任何新变化，则用来改写上一次commit的提交信息
$ git commit --amend -m [message]

# 重做上一次commit，并包括指定文件的新变化
$ git commit --amend <file1> <file2> ...
```

### 3.4 历史记录

```bash
$ git log
```

![image-20210407161144362](https://gitee.com/daxiang2008/doc/raw/master/Typora/img/image-20210407161144362.png)

```bash
$ git log --pretty=oneline
```

​	使用该命令可以仅查看标识

![image-20210407161355381](https://gitee.com/daxiang2008/doc/raw/master/Typora/img/image-20210407161355381.png)

### 3.5  版本退回

```bash
$ git reset --hard 版本号
```

​	在Git中，用HEAD表示当前版本（注意我的提交ID和你的肯定不一样），上一个版本就是HEAD^，上上一个版本就是HEAD^^，当然往上100个版本写100个^比较容易数不过来，所以写成HEAD~100。

​	那么退回上一个版本的命令就是

```bash
$ git reset --hard HEAD^
```

或者

``` bash
$ git reset --hard commit id/前5-6位
```

为什么可以很快的退回上一个或者其他状态呢，因为Git在内部有个指向当前版本的`HEAD`指针，当你回退版本的时候，Git仅仅是把HEAD从指向当前的。如图

![image-20210407163103229](https://gitee.com/daxiang2008/doc/raw/master/Typora/img/image-20210407163103229.png)

​	查看历史命令。如果退回到某一个状态后，又想回到当前最新的状态怎么办呢，通过git log是看不到的。

![image-20210408084540291](https://gitee.com/daxiang2008/doc/raw/master/Typora/img/image-20210408084540291.png)

必须通过reflog查看每次一次的命令，找到commit ID。

````bash
$ git reflog
````

![image-20210408084443010](https://gitee.com/daxiang2008/doc/raw/master/Typora/img/image-20210408084443010.png)

### 3.6 查看状态

​	在notepad中将redme.txt修改后，运行git status命令看看结果：

```bahs
$ git status
```

​	git status命令:可以让我们时刻掌握仓库当前的状态，显示“readme.txt”红色。表示readme.txt被修改过了，但还没有准备提交的修改。

![image-20210407160617027](https://gitee.com/daxiang2008/doc/raw/master/Typora/img/image-20210407160617027.png)

​	

​	然后git add redme.txt 同样没有任何输出。在执行第二步git commit之前，我们再运行git status看看当前仓库的状态：

![image-20210407160913679](https://gitee.com/daxiang2008/doc/raw/master/Typora/img/image-20210407160913679.png)

​	此时会发现已经变绿色了。

​	测试使用commit后的状态！！！！

### 3.6 撤销修改

​	如果文件修改后，你想撤回怎么办？

​	这个要分三种情况

​		1、修改了文件，但是没有add操作（就是未提交到暂存区）

​		2、文件已修改，且已经提交给了暂存区（add操作了）。

​		3、已经提交了（commit了）。

​	第一种，当你改乱了工作区某个文件的内容，想直接丢弃工作区的修改时，用如下命令。

```bash
$ git checkout -- readme.txt
```

```bash
$ git restore readme.txt
```

第二种，使用一下任一条命令，让文件退出暂存区到工作区。然后在执行第一种操作。

```bash
$ git reset HEAD readme.txt
```

​	第三种，直接使用3.5的版本退回即可！

reset总结

注：只对本地分支有效，对远程分支无效。

```bash
# 撤销上一次向暂存区添加的所有文件
$ git reset

# 同时撤销暂存区和工作区的修改，
# 回复到上一次提交的状态
$ git reset --hard

# 撤销上一次向暂存区添加的某个指定文件，
# 不影响工作区中的该文件
$ git reset -- <filename>
```

对于添加的新文件如何处理

```bash
$ git clean -f
```

### 3.7 添加多个文件

​	编码都在IDE中完成，所有往往都不止一个文件需要添加到仓库，那么如果要一次添加多个文件怎么办呢，使用

```bash
$ git add .
```

或

```bash
$ git add --all
```

或者

```bash
$ git add 文件1 文件2 .......
```

### 3.8 查看修改明细

如果要查看文件的具体哪一行修改了可以采用下面的命令。

```bash
$ git diff
```

![image-20210408084725173](https://gitee.com/daxiang2008/doc/raw/master/Typora/img/image-20210408084725173.png)

### 3.9 缓存工作区

`git stash`命令用于暂时保存没有提交的工作。运行该命令后，所有没有commit的代码，都会暂时从工作区移除，回到上次commit时的状态。

它处于`git reset --hard`（完全放弃还修改了一半的代码）与`git commit`（提交代码）命令之间，很类似于“暂停”按钮。

```bash
# 暂时保存没有提交的工作
$ git stash
Saved working directory and index state WIP on workbranch: 56cd5d4 Revert "update old files"
HEAD is now at 56cd5d4 Revert "update old files"

# 列出所有暂时保存的工作
$ git stash list
stash@{0}: WIP on workbranch: 56cd5d4 Revert "update old files"
stash@{1}: WIP on project1: 1dd87ea commit "fix typos and grammar"

# 恢复某个暂时保存的工作
$ git stash apply stash@{1}

# 恢复最近一次stash的文件
$ git stash pop

# 丢弃最近一次stash的文件
$ git stash drop

# 删除所有的stash
$ git stash clear
```

上面命令会将所有已提交到暂存区，以及没有提交的修改，都进行内部保存，没有将工作区恢复到上一次commit的状态。

使用下面的命令，取回内部保存的变化，它会与当前工作区的代码合并。

```bash
$ git stash pop
```

这时，如果与当前工作区的代码有冲突，需要手动调整。

`git stash`命令可以运行多次，保存多个未提交的修改。这些修改以“先进后出”的stack结构保存。

`git stash list`命令查看内部保存的多次修改。

```bash
$ git stash list
stash@{0}: WIP on new-feature: 5cedccc Try something crazy
stash@{1}: WIP on new-feature: 9f44b34 Take a different direction
stash@{2}: WIP on new-feature: 5acd291 Begin new feature
```

上面命令假设曾经运行过`git stash`命令三次。

`git stash pop`命令总是取出最近一次的修改，但是可以用`git stash apply`指定取出某一次的修改。

```bash
$ git stash apply stash@{1}
```

上面命令不会自动删除取出的修改，需要手动删除。

```bash
$ git stash drop stash@{1}
```

git stash 子命令一览。

```bash
# 展示目前存在的stash
$ git stash show -p

# 切换回stash
$ git stash pop

# 清除stash
$ git stash clear
```

### 3.10 忽略

有些时候，你必须把某些文件放到Git工作目录中，但又不能提交它们，比如保存了数据库密码的配置文件啦，等等，每次`git status`都会显示`Untracked files ...`，有强迫症的童鞋心里肯定不爽。

好在Git考虑到了大家的感受，这个问题解决起来也很简单，在Git工作区的根目录下创建一个特殊的`.gitignore`文件，然后把要忽略的文件名填进去，Git就会自动忽略这些文件。

忽略文件的原则是：

1. 忽略操作系统自动生成的文件，比如缩略图等；
2. 忽略编译生成的中间文件、可执行文件等，也就是如果一个文件是通过另一个文件自动生成的，那自动生成的文件就没必要放进版本库，比如Java编译产生的`.class`文件；
3. 忽略你自己的带有敏感信息的配置文件，比如存放口令的配置文件。

举个例子：

假设你在Windows下进行开发。

```bash
# Windows:
Thumbs.db
ehthumbs.db
Desktop.ini

# Python:
*.py[cod]
*.so
*.egg
*.egg-info
dist
build

# My configurations:
db.ini
deploy_key_rsa
```

最后一步就是把`.gitignore`也提交到Git，就完成了！当然检验`.gitignore`的标准是`git status`命令是不是说`working directory clean`。

```txt
以斜杠/开头表示目录；
以星号*通配多个字符；
以问号?通配单个字符
以方括号[]包含单个字符的匹配列表；
以叹号!表示不忽略(跟踪)匹配到的文件或目录；

此外，git 对于 .ignore 配置文件是按行从上到下进行规则匹配的，意味着如果前面的规则匹配的范围更大，则后面的规则将不会生效；
```



小结

- 忽略某些文件时，需要编写`.gitignore`；
- `.gitignore`文件本身要放到版本库里，并且可以对`.gitignore`做版本管理！

## 四、远程仓库

### 4.1 克隆远程仓库

​	假设当我们只有远程仓库而没有本地仓库时，我们可以采用克隆的方式获得远程仓库。要克隆一个仓库，首先必须知道仓库的地址，然后使用`git clone`命令克隆。此命令如下：

```bash
$ git clone 远程仓库地址
```

![image-20210408090720234](https://gitee.com/daxiang2008/doc/raw/master/Typora/img/image-20210408090720234.png)

​	如果有多个人协作开发，那么每个人各自从远程克隆一份就可以了。

​	也许你还注意到，我们克隆的地址是https开头的。实际上，Git支持多种协议，默认的`git://`使用ssh，但也可以使用`https`等其他协议。

​	使用`https`除了速度慢以外，还有个最大的麻烦是每次推送都必须输入口令，但是在某些只开放http端口的公司内部就无法使用`ssh`协议而只能用`https`。

### 4.2 创建远程仓库

​	如果你有本地仓库，需要将本地的仓库和远程仓库关联并支持以后推送或者下载，那么在首次使用时需要做以下两件事情（只做一次）。

**（1）配置创建SSH key（用于识别用户，免得每次输入账号密码）**

​	在命令窗口输入，然后一路回车，不需要输入，如果需要多个公钥，则要加参数(-f ~/.ssh/id_rsa_名称)。

```bash
$ ssh-keygen–t rsa –C "你的邮箱"
例如
$ ssh-keygen –t rsa –C 32222@qq.com
```

![image-20210408085539826](https://gitee.com/daxiang2008/doc/raw/master/Typora/img/image-20210408085539826.png)

​	完成后，在你的仓库中就会有.ssh的文件夹，同时会有公钥、秘钥 、host

![image-20210601090154059](https://gitee.com/daxiang2008/doc/raw/master/Typora/img/image-20210601090154059.png)

**（2）添加公钥**

​	将公钥id_rsa.pub中的**内容**添加到git的远程网站中。gitee，github或者其他提供互联网git服务的网站中。

​	我们下面以gitee为例。使用GitHub时，国内的用户经常遇到的问题是访问速度太慢，有时候还会出现无法连接的情况。如果我们希望体验Git飞一般的速度，可以使用国内的Git托管服务——[Gitee](https://gitee.com/?utm_source=blog_lxf)（[gitee.com](https://gitee.com/?utm_source=blog_lxf)）。和GitHub相比，Gitee也提供免费的Git仓库。此外，还集成了代码质量检测、项目演示等功能。对于团队协作开发，Gitee还提供了项目管理、代码托管、文档管理的服务，5人以下小团队免费。


​	点击“确定”即可完成并看到刚才添加的Key：

![gitee-key](https://www.liaoxuefeng.com/files/attachments/1163453163108928/l)

**（3）创建远程仓库**

​	点击主页面右上角的"+"

![image-20210408092038904](https://gitee.com/daxiang2008/doc/raw/master/Typora/img/image-20210408092038904.png)

![image-20210408092202154](https://gitee.com/daxiang2008/doc/raw/master/Typora/img/image-20210408092202154.png)

### 4.3 关联远程仓库

项目名称最好与本地库保持一致：然后，我们在本地库上使用命令`git remote add`把它和Gitee的远程库关联：

```bash
$ git remote add origin 远程仓库地址
```

如：

```bash
$ git remote add origin git@gitee.com:liaoxuefeng/learngit.git
```

之后，就可以正常地用`git push`和`git pull`推送了！

如果在使用命令`git remote add`时报错：

```bash
$ git remote add origin git@gitee.com:liaoxuefeng/learngit.git
  fatal: remote origin already exists.
```

这说明本地库已经关联了一个名叫`origin`的远程库，此时，可以先用`git remote -v`查看远程库信息：

```bash
$ git remote -v
  origin	git@github.com:michaelliao/learngit.git (fetch)
  origin	git@github.com:michaelliao/learngit.git (push)
```

可以看到，本地库已经关联了`origin`的远程库，并且，该远程库指向GitHub。

我们可以删除已有的GitHub远程库：

```bash
$ git remote rm origin
```

再关联Gitee的远程库（注意这里采用的ssh地址，其他合法协议地址均可）：

```bash
$ git remote add origin git@gitee.com:liaoxuefeng/learngit.git
```

此时，我们再查看远程库信息：

```bash
$ git remote -v
origin	git@gitee.com:liaoxuefeng/learngit.git (fetch)
origin	git@gitee.com:liaoxuefeng/learngit.git (push)
```

### 4.4 关联两个远程

如果需要关联两个，可以采用如下操作，注意远程库的名称叫`gitee`，不叫`origin`，origin是关联一个仓库时的默认名称。

```bash
$ git clone git@server:/srv/sample.git
```

```bash
$ git remote add gitee git@gitee.com:liaoxuefeng/learngit.git
```

然后关联

```bash
$ git remote add github git@github.com:michaelliao/learngit.git
```

现在，我们用`git remote -v`查看远程库信息，可以看到两个远程库：

```bash
$ git remote -v
    gitee	git@gitee.com:liaoxuefeng/learngit.git (fetch)
    gitee	git@gitee.com:liaoxuefeng/learngit.git (push)
    github	git@github.com:michaelliao/learngit.git (fetch)
    github	git@github.com:michaelliao/learngit.git (push)
```

### 4.5  远程仓库的推送、拉取

如果要推送到GitHub，使用命令：

```
$ git push github master
```

如果要推送到Gitee，使用命令：

```
$ git push gitee master
```

这样一来，我们的本地库就可以同时与多个远程库互相同步。

```bash
# 下载远程仓库的所有变动
$ git fetch [remote]

# 显示所有远程仓库
$ git remote -v

# 显示某个远程仓库的信息
$ git remote show [remote]

# 增加一个新的远程仓库，并命名
$ git remote add [shortname] [url]

# 取回远程仓库的变化，并与本地分支合并
$ git pull [remote] [branch]

# 上传本地指定分支到远程仓库
$ git push [remote] [branch]

# 强行推送当前分支到远程仓库，即使有冲突
$ git push [remote] --force

# 推送所有分支到远程仓库
$ git push [remote] --all
```

**问题1、**：如果无法拉取或者推送

![image-20210417235946296](https://gitee.com/daxiang2008/doc/raw/master/Typora/img/image-20210417235946296.png)

这个是远程仓库权限的问题，进入到远程仓库所在的目录，执行

```bash
chown -R git:git test.git
```



**问题2、**：如果远程仓库非空，则推送会出现冲突,则需要拉取远程代码进行本地融合，并解决冲突后push。

```bash
$ git pull <标记名称> master
```

**问题3、**：如果本地仓库不为空，新建的远程仓库也不为空，需要将远程的拉取（前提已经本地已经绑定了远程地址）到本地合并后，再推送！
```
$ git pull origin master --allow-unrelated-histories 
```

### 4.6 推送所有分支到远程仓库

```bash
# remote 远程分支
$ git push [remote] --all
```

## 五、分支

​	在版本回退里，你已经知道，每次提交，Git都把它们串成一条时间线，这条时间线就是一个分支。截止到目前，只有一条时间线，在Git里，这个分支叫主分支，即master分支。HEAD严格来说不是指向提交，而是指向master，master才是指向提交的，所以，HEAD指向的就是当前分支。

### 5.1 什么是分支？



![image-20210408100945238](https://gitee.com/daxiang2008/doc/raw/master/Typora/img/image-20210408100945238.png)

​	当我们创建新的分支，例如dev时，Git新建了一个指针叫dev，指向master相同的提交，再把HEAD指向dev，就表示当前分支在dev上：

![image-20210408101000786](https://gitee.com/daxiang2008/doc/raw/master/Typora/img/image-20210408101000786.png)

​	Git创建一个分支很快，因为除了增加一个`dev`指针，改改`HEAD`的指向，工作区的文件都没有任何变化！不过，从现在开始，对工作区的修改和提交就是针对`dev`分支了，比如新提交一次后，`dev`指针往前移动一步，而`master`指针不变：

![image-20210408101021516](https://gitee.com/daxiang2008/doc/raw/master/Typora/img/image-20210408101021516.png)

​	假如我们在`dev`上的工作完成了，就可以把`dev`合并到`master`上。Git怎么合并呢？最简单的方法，就是直接把`master`指向`dev`的当前提交，就完成了合并：

![image-20210408101032846](https://gitee.com/daxiang2008/doc/raw/master/Typora/img/image-20210408101032846.png)

​	所以Git合并分支也很快！就改改指针，工作区内容也不变！合并完分支后，甚至可以删除`dev`分支。删除`dev`分支就是把`dev`指针给删掉，删掉后，我们就剩下了一条`master`分支：

![image-20210408101048130](https://gitee.com/daxiang2008/doc/raw/master/Typora/img/image-20210408101048130.png)

### 5.2 创建分支

首先，我们创建`dev`分支，然后切换到`dev`分支：

切换分支的命令：

```bash
$ git checkout -b dev
```

或

```bash
$ git switch -c <branchName>
```

目前由于checkout可使用的位置较多，所以更改了切换命令

```bash
$ git switch -c <branchName>
举例：git switch testBranch
```

![image-20210408101151511](https://gitee.com/daxiang2008/doc/raw/master/Typora/img/image-20210408101151511.png)

该命令相当于两个命令，1、创建dev分支   2、切换到dev分支

```bash
$ git branch dev
$ git checkout dev
Switched to branch 'dev'
```

在远程主机`origin`上创建一个`dev`的分支，并与本地的同名分支建立追踪关系。

```bash
$ git push -u origin dev
```

查看分支，`git branch`命令会列出所有分支，当前分支前面会标一个`*`号。

```bash
$ git branch
```

![image-20210408101234589](https://gitee.com/daxiang2008/doc/raw/master/Typora/img/image-20210408101234589.png)

然后，我们就可以在`dev`分支上正常提交，比如对`readme.txt`做个修改，加上一行：

```bash
Creating a new branch is quick.
```

然后提交：

```bash
$ git add readme.txt 
$ git commit -m "branch test"
[dev b17d20e] branch test
 1 file changed, 1 insertion(+)
```

### 5.3 合并分支

现在，`dev`分支的工作完成，我们就可以切换回`master`分支：

```bash
$ git checkout master
Switched to branch 'master'
```

切换回`master`分支后，再查看一个`readme.txt`文件，刚才添加的内容不见了！因为那个提交是在`dev`分支上，而`master`分支此刻的提交点并没有变：
现在，我们把`dev`分支的工作成果合并到`master`分支上：

```bash
$ git merge dev
Updating d46f35e..b17d20e
Fast-forward
 readme.txt | 1 +
 1 file changed, 1 insertion(+)
```

`git merge`命令用于合并指定分支到当前分支。合并后，再查看`readme.txt`的内容，就可以看到，和`dev`分支的最新提交是完全一样的。注意到上面的`Fast-forward`信息，Git告诉我们，这次合并是“快进模式”，也就是直接把`master`指向`dev`的当前提交，所以合并速度非常快。当然，也不是每次合并都能`Fast-forward`。

#### git merge

将当前分支合并到指定分支。

```bash
$ git merge develop
```

将当前分支与develop分支合并，产生的新的commit对象有两个父节点。

如果“指定分支”本身是当前分支的一个直接子节点，则会产生fast-forward合并，即合并不会产生新的节点，只是让当前分支指向“指定分支”的最新commit。

Git合并所采用的方法是Three-way merge，及合并的时候除了要合并仓库，加上它们共同的父节点。这样可以大大減少人为处理 情況。如果采用two-way merge，則只用两个仓库进行合并（svn默认就是这种合并方法。）

注意：fast-forward必须是源分支和目标分支没有分叉。如果是下图中的场景，则无法通过快速合并。

[<img src="https://gitee.com/michael_xiang/images/raw/master/uPic/RQ39Rv.png" alt="分叉" style="zoom:80%;" />](https://gitee.com/michael_xiang/images/raw/master/uPic/RQ39Rv.png)

#### fast-forward 与 --no-ff 的区别

假如有一个场景：有两个分支，master 分支和 feature 分支。现在，feautre 分支需要合并回 master 分支。

[<img src="https://gitee.com/michael_xiang/images/raw/master/uPic/wOR7JK.png" alt="fast-forward-初始状态" style="zoom:80%;" />](https://gitee.com/michael_xiang/images/raw/master/uPic/wOR7JK.png)

`fast-forward` 合并方式是**条件允许**的情况，通过将 master 分支的 HEAD 位置移动到 feature 分支的最新提交点上，这样就实现了快速合并。这种情况，是不会新生成 commit 的。

[<img src="https://gitee.com/michael_xiang/images/raw/master/uPic/QpdH5g.png" alt="fast-forward" style="zoom:80%;" />](https://gitee.com/michael_xiang/images/raw/master/uPic/QpdH5g.png)

`--no-ff` 的方式进行合并，master 分支就会新生成一次提交记录。

[<img src="https://gitee.com/michael_xiang/images/raw/master/uPic/BIqlQW.png" alt="--no-ff" style="zoom:80%;" />](https://gitee.com/michael_xiang/images/raw/master/uPic/BIqlQW.png)

> 如果条件满足时，merge 默认采用的 `fast-forward` 方式进行合并，除非你显示的加上 `--no-ff` 选项；而条件不满足时，merge 也是无法使用 `fast-forward` 合并成功的！

#### git rebase

`rebase` 命令是一个经常听到，但是大多数人掌握又不太好的一个命令。`rebase` 合并往往又被称为 「变基」。「变基」就是改变当前分支的起点。**注意，是当前分支！** `rebase` 命令后面紧接着的就是「基分支」。

变基前：

[<img src="https://gitee.com/michael_xiang/images/raw/master/uPic/RQ39Rv.png" alt="分叉" style="zoom:80%;" />](https://gitee.com/michael_xiang/images/raw/master/uPic/RQ39Rv.png)

`git reabse master feature` 变基后：

[<img src="https://gitee.com/michael_xiang/images/raw/master/uPic/HepjTM.png" alt="变基后" style="zoom:80%;" />](https://gitee.com/michael_xiang/images/raw/master/uPic/HepjTM.png)

> git rebase 命令通常称为向前移植（`forward porting`）。

#### git rebase示例

我们接下来进行实际的测试，将代码库状态构造成分叉的状态，状态图如下：

[![分叉初始状态](https://gitee.com/michael_xiang/images/raw/master/uPic/o9hGJa.png)](https://gitee.com/michael_xiang/images/raw/master/uPic/o9hGJa.png)

以 master 分支为基，对 feautre 分支进行变基：

```undefined
git checkout feature
git rebase master
```

以上两行命令，其实可以简写为：`git rebase master feature`

> 特性分支 feature 向前移植到了 master 分支。经常使用 git rebase 操作把本地开发分支移植到远端的 `origin` 追踪分支上。也就是经常说的，「把你的补丁变基到 xxx 分支的头」

[![变基后](https://gitee.com/michael_xiang/images/raw/master/uPic/8jMmax.png)](https://gitee.com/michael_xiang/images/raw/master/uPic/8jMmax.png)

可以发现，在 master 分支的最新节点（`576cb7b`）后面多了 2 个提交（生成了新的提交记录，仅仅提交信息保持一致），而这两个提交内容就是来自变基前 feature 分支，feature 分支的提交历史发生了改变。

观察上图还可以发现，变基后，改变的只是 feature 分支，基分支（master 分支）的 HEAD 指针依然在之前的 commit （`576cb7b`）处。这时候要将 feature 分支合入到 master 分支上，就满足 `fast-forward` 的条件了，`master` 分支执行快速合并，将 HEAD 指针指向刚刚最新合入的提交点：

```sql
git checkout master
git merge feature
```

[![快速合并](https://gitee.com/michael_xiang/images/raw/master/uPic/BUiz44.png)](https://gitee.com/michael_xiang/images/raw/master/uPic/BUiz44.png)

看下图 master 分支图，观察 HEAD 指针的位置：
[![分支图](https://gitee.com/michael_xiang/images/raw/master/uPic/dgYPmX.png)](https://gitee.com/michael_xiang/images/raw/master/uPic/dgYPmX.png)

rebase 变基操作最适合的是本地分支和远端对应跟踪分支之间的合并。这样理解可能会更清晰一点。比如，远端仓库里有一个特性分支 feature，除了你开发之外，还有其他人往这个分支进行合入。当你每次准备提交到远端之前，其实可以尝试变基，这时候基分支就是远端的追踪分支。

下图是仓库的分支图：

[![与远端分支分叉](https://gitee.com/michael_xiang/images/raw/master/uPic/PDdv1I.png)](https://gitee.com/michael_xiang/images/raw/master/uPic/PDdv1I.png)

```bash
git fetch
git rebase origin/feature feature
```

[![变基后](https://gitee.com/michael_xiang/images/raw/master/uPic/UBCOSo.png)](https://gitee.com/michael_xiang/images/raw/master/uPic/UBCOSo.png)

观察上图，我们本地的提交以远端分支的最新提交为「基」，将差异提交重新进行了提交！远端分支的提交记录依然是一条直线~如果分叉的情况，不采用这种「变基操作」，而直接采用 `merge` 的方式合并，就会有如下这种分支提交图：

[![no-ff 合并](https://gitee.com/michael_xiang/images/raw/master/uPic/hkVfzT.png)](https://gitee.com/michael_xiang/images/raw/master/uPic/hkVfzT.png)

因为分叉了，采用 `git pull` 时也没法 `fast-forward` 合并，只能采用 `no-ff` 方式合并，最后的提交历史就会像上图那样。会产生一个合并提交。同时，分支图也显得稍微杂乱了一点，因为 feature 分支不是一条直线了。但是，其实也有好处，可以实际的看出来合并的提交历史。该选择哪个，往往取决于团队的选择策略。

压缩提交

```
git rebase -i HEAD~3   //3表示有3个commit
```

#### git rebase总结

`rebase` 命令其实关键在于理解「基」，`git rebase <基分支>`，就是将当前基分支与当前分支的差异提交获取到，然后在「基分支」最新提交点后面将差异提交逐个再次提交，最后将当前分支的 HEAD 指针指向最新的提交点。

「基分支」的 HEAD 位置是不变的。要想完成分支合并，完成变基之后，需要再进行分支间的合并等操作。

rebase 命令的用法也不止于此，计划后期会专门写一篇介绍她的文章。本文本来是计划介绍 merge 命令的，但是合并的方式中，其实也经常涉及变基操作之后的合并，因此，干脆就放一起比较好了，这样易于理解记忆。

### 5.4 删除分支

合并完成后，就可以放心地删除`dev`分支了，前提是该分支没有未合并的变动。

```bash
$ git branch -d dev
Deleted branch dev (was b17d20e).
```

强制删除`MyBranch`分支，不管有没有未合并变化。

```bash
$ git branch -D dev
```

### 5.5 小结

查看分支：`git branch`

创建分支：`git branch <name>`

切换分支：`git checkout <name>`或者`git switch <name>`

创建+切换分支：`git checkout -b <name>`或者`git switch -c <name>`

合并某分支到当前分支：`git merge <name>`

删除分支：`git branch -d <name>`

分支合并图：`git log --graph`

 `master`分支是主分支，因此要时刻与远程同步；

`dev`分支是开发分支，团队所有成员都需要在上面工作，所以也需要与远程同步；

 `bug`分支只用于在本地修复bug，就没必要推到远程；

`feature`分支是否推到远程，取决于你是否和你的小伙伴合作在上面开发。

## 六、解决冲突

### 6.1分支合并冲突

创建一个新的分支并切换：

```bash
$ git switch -c dev
```

修改`readme.txt`最后一行，改为：

```bash
Creating a new branch is quick AND simple.
```

在`dev`分支上提交：

```bash
$ git add readme.txt

$ git commit -m "AND simple"
 1 file changed, 1 insertion(+), 1 deletion(-)
```

切换到`master`分支：

```bash
$ git switch master
Switched to branch 'master'
Your branch is ahead of 'origin/master' by 1 commit.
  (use "git push" to publish your local commits)
```

Git还会自动提示我们当前`master`分支比远程的`master`分支要超前1个提交。

在`master`分支上把`readme.txt`文件的最后一行改为：

```bash
Creating a new branch is quick & simple.
```

提交：

```bash
$ git add readme.txt 
$ git commit -m "& simple"
[master 5dc6824] & simple
 1 file changed, 1 insertion(+), 1 deletion(-)
```

现在，`master`分支和`dev`分支各自都分别有新的提交，变成了这样：

![image-20210601093109870](https://gitee.com/daxiang2008/doc/raw/master/Typora/img/image-20210601093109870.png)

这种情况下，Git无法执行“快速合并”，只能试图把各自的修改合并起来，但这种合并就可能会有冲突，我们试试看：

```bash
$ git merge dev
Auto-merging readme.txt
CONFLICT (content): Merge conflict in readme.txt
Automatic merge failed; fix conflicts and then commit the result.
```

果然冲突了！Git告诉我们，`readme.txt`文件存在冲突，必须手动解决冲突后再提交。`git status`也可以告诉我们冲突的文件：

```bash
$ git status
On branch master
Your branch is ahead of 'origin/master' by 2 commits.
  (use "git push" to publish your local commits)

You have unmerged paths.
  (fix conflicts and run "git commit")
  (use "git merge --abort" to abort the merge)

Unmerged paths:
  (use "git add <file>..." to mark resolution)

	both modified:   readme.txt

no changes added to commit (use "git add" and/or "git commit -a")
```

我们可以直接查看readme.txt的内容：

```bash
Git is a distributed version control system.
Git is free software distributed under the GPL.
Git has a mutable index called stage.
Git tracks changes of files.
<<<<<<< HEAD
Creating a new branch is quick & simple.
=======
Creating a new branch is quick AND simple.
>>>>>>> dev
```

Git用`<<<<<<<`，`=======`，`>>>>>>>`标记出不同分支的内容，我们修改如下后保存：

```bash
Creating a new branch is quick and simple.
```

再提交：

```bash
$ git add .
$ git commit -m "conflict fixed"
[master cf810e4] conflict fixed
```

现在，`master`分支和`dev`分支变成了下图所示：

![image-20210601093321357](https://gitee.com/daxiang2008/doc/raw/master/Typora/img/image-20210601093321357.png)


用带参数的`git log`也可以看到分支的合并情况：

```bash
$ git log --graph
*   cf810e4 (HEAD -> master) conflict fixed
|\  
| * 14096d0 (dev) AND simple
* | 5dc6824 & simple
|/  
* b17d20e branch test
* d46f35e (origin/master) remove test.txt
* b84166e add test.txt
* 519219b git tracks changes
* e43a48b understand how stage works
* 1094adb append GPL
* e475afc add distributed
* eaadf4e wrote a readme file
```
### 6.2本地远程冲突

在多人开发时会出现进度不一致的问题，那么也就会出现合并代码时可能出现冲突。

### 6.3参与协作

下载开发分支

方式一

只clone dev分支，本地无master

```
git clone -b dev https://gitee.com/daxiang2008/demo.git
```

方式二

先克隆master，在clone dev

```
git clone https://gitee.com/daxiang2008/demo.git
git checkout -b dev origin/dev
```

### 6.4 fixbug

先在创建新分支，修复bug

```
$ git checkout -b issue-101
Switched to a new branch 'issue-101'
```

修复后提交

```
$ git add readme.txt 
$ git commit -m "fix bug 101"
[issue-101 4c805e2] fix bug 101
 1 file changed, 1 insertion(+), 1 deletion(-)
```

切换到主分支

```
$ git switch master
Switched to branch 'master'
Your branch is ahead of 'origin/master' by 6 commits.
  (use "git push" to publish your local commits)

$ git merge --no-ff -m "merged bug fix 101" issue-101
Merge made by the 'recursive' strategy.
 readme.txt | 2 +-
 1 file changed, 1 insertion(+), 1 deletion(-)
```

然后切换到正在开发的分支

```
$ git switch dev
Switched to branch 'dev'

$ git status
On branch dev
nothing to commit, working tree clean

```

提交修复

```
$ git branch
* dev
  master
  
$ git cherry-pick 4c805e2
[master 1d4b803] fix bug 101
 1 file changed, 1 insertion(+), 1 deletion(-)
```

出现问题

如果使用cherry-pick出现冲突，那么解决冲突后，需要使用 `git add` 命令将解决后的文件标记为已解决，然后再使用`git cherry-pick --continue`命令继续应用 Cherry-pick。

或者使用参数

```
$ git cherry-pick --abort           发生代码冲突后，退出Cherry pick，回到操作前的样子

$ git cherry-pick --quit            发生代码冲突后，退出Cherry pick，不回到操作前的样子
```





## 七、标签

```bash
# 列出所有tag
$ git tag

# 新建一个tag在当前commit
$ git tag [tag]

# 新建一个tag在指定commit
$ git tag [tag] [commit]

# 删除一个标签
$ git tag -d [tag]

# 查看tag信息
$ git show [tag]

# 提交指定tag
$ git push origin [tag]

# 提交所有tag
$ git push origin --tags

# 新建一个分支，指向某个tag
$ git checkout -b [branch] [tag]
```

### 小结

- 命令`git push origin <tagname>`可以推送一个本地标签；
- 命令`git push origin --tags`可以推送全部未推送过的本地标签；
- 命令`git tag -d <tagname>`可以删除一个本地标签；

## 八、工作流

​	工作流有各式各样的用法，但也正因此使得在实际工作中如何上手使用增加了难度。这篇指南通过总览公司团队中最常用的几种 Git 工作流让大家可以上手使用。在阅读的过程中请记住，本文中的几种工作流是作为方案指导而不是条例规定。在展示了各种工作流可能的用法后，你可以从不同的工作流中挑选或揉合出一个满足你自己需求的工作流。

### 8.1工作流分类

- 集中式工作流

  如果你的开发团队成员已经很熟悉 Subversion，集中式工作流让你无需去适应一个全新流程就可以体验 Git 带来的收益。这个工作流也可以作为向更 Git 风格工作流迁移的友好过渡。

  ![image-20210408100705555](https://gitee.com/daxiang2008/doc/raw/master/Typora/img/image-20210408100705555.png)

- 功能分支工作流

  功能分支工作流以集中式工作流为基础，不同的是为各个新功能分配一个专门的分支来开发。这样可以在把新功能集成到正式项目前，用 Pull Requests 的方式讨论变更。

  ![image-20210408100720653](https://gitee.com/daxiang2008/doc/raw/master/Typora/img/image-20210408100720653.png)

- GitFlow 工作流

  GitFlow 工作流通过为功能开发、发布准备和维护分配独立的分支，让发布迭代过程更流畅。严格的分支模型也为大型项目提供了一些非常必要的结构。

  ![image-20210408100731114](https://gitee.com/daxiang2008/doc/raw/master/Typora/img/image-20210408100731114.png)

- Forking 工作流

  Forking 工作流是分布式工作流，充分利用了 Git 在分支和克隆上的优势。可以安全可靠地管理大团队的开发者（developer），并能接受不信任贡献者（contributor）的提交。

  ![image-20210408100740116](https://gitee.com/daxiang2008/doc/raw/master/Typora/img/image-20210408100740116.png)

- Pull Requests

  Pull requests 让开发者更方便地进行协作的功能，提供了友好的 Web 界面可以在提议的修改合并到正式项目之前对修改进行讨论。

![image-20210408100750516](https://gitee.com/daxiang2008/doc/raw/master/Typora/img/image-20210408100750516.png)

### 分支策略

在实际开发中，我们应该按照几个基本原则进行分支管理：

首先，`master`分支应该是非常稳定的，也就是仅用来发布新版本，平时不能在上面干活；

那在哪干活呢？干活都在`dev`分支上，也就是说，`dev`分支是不稳定的，到某个时候，比如1.0版本发布时，再把`dev`分支合并到`master`上，在`master`分支发布1.0版本；

你和你的小伙伴们每个人都在`dev`分支上干活，每个人都有自己的分支，时不时地往`dev`分支上合并就可以了。

所以，团队合作的分支看起来就像这样：

![image-20240416085728176](https://gitee.com/daxiang2008/pic/raw/master/img/image-20240416085728176.png)

### master 分支

- master 为产品主分支，该分支为只读唯一分支，也是用于部署生产环境的分支，需确保master分支的稳定性。
- master 分支一般由release分支或hotfix分支合并，任何情况下都不应该直接修改master分支代码。
- 产品的功能全部实现后，最终在master分支对外发布，另外所有在master分支的推送应该打标签（tag）做记录，方便追溯。
- master 分支不可删除。

### develop 分支

- develop 为主开发分支，基于master分支创建，始终保持最新完成功能的代码以及bug修复后的代码。
- develop 分支为只读唯一分支，只能从其他分支合并，不可以直接在该分支做功能开发或bug修复。一般开发新功能时，feature分支都是基于develop分支下创建的。
- develop 分支包含所有要发布到下一个release的代码。feature功能分支完成后,
- 开发人员需合并到develop分支(不推送远程)，需先将develop分支合并到feature，解决完冲突后再合并到develop分支。当所有新功能开发完成后，开发人员并自测完成后，此时从develop拉取release分支，进行提测。
- release或hotfix 分支上线完成后, 开发人员需合并到develop分支并推送远程。
- develop 分支不可删。
## 九、常用的linux命令

### 文件和目录管理命令

**ls**：列出当前目录中的文件和子目录。 

**pwd**：显示当前工作目录的路径。

**cd**：切换工作目录。 

**mkdir**：创建新目录。 

**rmdir**：删除空目录。 *rmdir directory_name*

**rm**：删除文件或目录。 *rm file_name rm -r directory_name # 递归删除目录及其内容*

**cp**：复制文件或目录。

**mv**：移动或重命名文件或目录。 *mv old_name new_name*

**touch**：创建空文件或更新文件的时间戳。 *touch file_name*

**cat**：连接和显示文件内容。 *cat file_name*



### 系统信息和网络命令

**ps**：显示当前运行的进程。 *ps aux*

**kill**：终止进程。 *kill process_id*

**ifconfig/ip**：查看和配置网络接口信息。 *ifconfig ip addr show*

**ping**：测试与主机的连通性。 *ping host_name_or_ip*

**wget/curl**：从网络下载文件。 *wget URL curl -O URL*

**chmod**：修改文件或目录的权限。 *chmod permissions file_name*

**chown**：修改文件或目录的所有者。 *chown owner:group file_name*

**tar**：用于压缩和解压文件和目录。 *tar -czvf archive.tar.gz directory_name # 压缩目录 tar -xzvf archive.tar.gz # 解压文件*

**df/du**：显示磁盘使用情况。 *df -h # 显示磁盘空间使用情况 du -h directory_name # 显示目录的磁盘使用情况*



### 用户和权限管理命令

**useradd/userdel**：用于添加和删除用户账户。 *useradd new_user # 添加用户 userdel username # 删除用户*

**passwd**：更改用户密码。 *passwd username*

**sudo**：以超级用户权限运行命令。 *sudo command_to_run_as_superuser*



### 远程访问和文件传输命令

**ssh**：远程登录到其他计算机。 *ssh username@remote_host*

**scp**：安全地将文件从本地复制到远程主机，或从远程主机复制到本地。 *scp local_file remote_user@remote_host:/remote/directory*



